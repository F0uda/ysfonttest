﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace fontSupport
{
    internal class eventLogger
    {
        private System.Diagnostics.EventLog eventLog1;
        private String _previx = "";
        public eventLogger(String prefix)
        {
            _previx = prefix;
            eventLog1 = new EventLog();
            if (!EventLog.SourceExists("fontSource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "fontSource", "testLog");
            }
            eventLog1.Source = "fontSource";
            eventLog1.Log = "testLog";
        }

        public void logIt(String msg)
        {
            eventLog1.WriteEntry(_previx + "; " + msg);
        }
    }
}
