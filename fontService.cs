﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

using fontSupport;

namespace fontServiceTest
{
    public partial class fontService : ServiceBase
    {
        private nakedTest _tester;

        private IntPtr dllHandle = IntPtr.Zero;

        public fontService()
        {
            InitializeComponent();
            _tester = new nakedTest(nakedTest.exeType.service);
        }
        ~fontService()
        {
            _tester.release();
        }


        protected override void OnStart(string[] args)
        {
            _tester.Run();
            return;
            /*
             * this call is fontTestStandard.cs
            doTest(@"d:\projects\ysh21_productionData\DebenuPDFLibrary64DLL1212.dll",
                    "jf8tq95o4w376b88m6mz6pb3y",
                    @"d:\temp",
                    @"d:\projects\ysh21_productionData\fonts\FREESCPT.TTF");
                    */
        }

        protected override void OnStop()
        {
        }
    }
}
