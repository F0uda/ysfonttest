﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DebenuPDFLibraryDLL1212;


namespace fontServiceTest
{
    class fontTestStandard
    {
        private PDFLibrary _qpdf = null;

        private eventLogger _logger;

        public fontTestStandard()
        {
            _logger = new eventLogger();
        }
        private void doTest(String libpath, String libkey, String tmpPath, String signatureFontFile)
        {
            try
            {
                _qpdf = new PDFLibrary(libpath);
            }
            catch (System.Exception ex)
            {
                _logger.logIt(String.Format("Cannot instantiate quickpdflib {0}\tError:{1}", new Object[] { libpath, ex.Message }));
            }

            int result = _qpdf.UnlockKey(libkey);          ///*laptopkey*/ desktopkey); 

            if (result == 0)
            {
                _qpdf = null;
                _logger.logIt(String.Format("Cannot register quick pdf lib. Library {0} LicenseKey:{1}", new Object[] { libpath, libkey }));
            }

            if (_qpdf.SetTempPath(tmpPath) == 0)
            {
                _logger.logIt("pdfapi: failure setting temporary directory " + tmpPath);
            }

            try
            {
                byte[] stream = File.ReadAllBytes(signatureFontFile);
                if (stream == null)
                    _logger.logIt("failure reading font file to stream");

                int fontid = _qpdf.AddTrueTypeFontFromString(stream);

            }
            catch (Exception e)
            {
                _logger.logIt("1:" + e.Message);

            }
        }

    }
}
