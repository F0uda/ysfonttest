﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace fontSupport
{
    internal class nakedTest
    {
        public enum exeType
        {
            executable,
            service
        };

        [DllImport("kernel32.dll")]
        internal static extern IntPtr LoadLibrary(String dllname);

        [DllImport("kernel32.dll")]
        internal static extern int FreeLibrary(IntPtr hModule);

        [DllImport("kernel32.dll")]
        internal static extern IntPtr GetProcAddress(IntPtr hModule, String procname);

        internal delegate int DelegateI();
        internal delegate IntPtr DelegateBII(int P1, int P2);
        internal delegate int DelegateIIBBI(int P1, IntPtr P2, IntPtr P3, int P4);
        internal delegate int DelegateIIBI(int P1, IntPtr P2, int P3);
        internal delegate int DelegateIIB(int P1, IntPtr P2);
        internal delegate int DelegateIIW(int P1, [MarshalAs(UnmanagedType.LPWStr)] string P2);
        internal delegate int DelegateIIWI(int P1, [MarshalAs(UnmanagedType.LPWStr)] string P2, int P3);
        internal delegate IntPtr DelegateWI(int P1);
        internal delegate int DelegateII(int P1);

        private DelegateI pfCreateLibrary;
        private DelegateBII pfCreateBuffer;
        private DelegateIIBBI pfAddToBuffer;
        private DelegateIIB pfAddTrueTypeFontFromString;
        private DelegateIIBI pfAddTrueTypeFontFromStringЕх;
        private DelegateIIWI pfAddTrueTypeFontFromFileEx;
        private DelegateIIB pfReleaseBuffer;
        private DelegateIIW pfUnlockKey;
        private DelegateII pfLibraryStringResultLength;
        private DelegateIIW pfSetTempPath;

        private DelegateWI pfGetTempPath;

        private eventLogger _logger;

        private void Release()
        {
            if (dllHandle == IntPtr.Zero)
                return;
            FreeLibrary(dllHandle);
            dllHandle = IntPtr.Zero;
        }

        private IntPtr dllHandle = IntPtr.Zero;
        private int instanceID = -1;

        public nakedTest(exeType exetype)
        {
            _logger = new eventLogger(exetype == exeType.executable ? "exe" : "service");
            _logger.logIt(RuntimeInformation.OSArchitecture.ToString());
        }

        public void release ()
        {
            if (dllHandle == IntPtr.Zero)
                return;
            FreeLibrary(dllHandle);
            dllHandle = IntPtr.Zero;
        }
        
        public void EnsureKWLLibrariesAreAll64Bit()
        {
            var assemblies = Assembly.GetExecutingAssembly().GetReferencedAssemblies().Where(x => x.FullName.StartsWith("fontService")).ToArray();
            foreach (var assembly in assemblies)
            {
                String assemblyname = assembly.FullName.Split(',')[0];
                var myAssemblyName = AssemblyName.GetAssemblyName(assemblyname + ".dll");
                _logger.logIt(String.Format("{0}:{1}", assemblyname, myAssemblyName.ProcessorArchitecture.ToString()));
            }
        }

        public void Run()
        {
//            EnsureKWLLibrariesAreAll64Bit();

             String p = AppDomain.CurrentDomain.BaseDirectory;
            String dllfile = p + @"PDFLibrary64DLL1711.dll"; //@"PDFLibrary64DLL1212.dll";
            _logger.logIt("start: " + dllfile);

            doNakedTests(dllfile, "jf8tq95o4w376b88m6mz6pb3y", p + @"\FREESCPT.TTF");

            return;
            /*
             * this call is fontTestStandard.cs
            doTest(@"d:\projects\ysh21_productionData\DebenuPDFLibrary64DLL1212.dll",
                    "jf8tq95o4w376b88m6mz6pb3y",
                    @"d:\temp",
                    @"d:\projects\ysh21_productionData\fonts\FREESCPT.TTF");
                    */
        }

        private void doNakedTests(String dllFileName, String libkey, String signatureFontFile)
        {
            try
            {
                _logger.logIt("loading Library");
                dllHandle = LoadLibrary(dllFileName);
                _logger.logIt("loaded. Handle:" + dllHandle.ToInt64().ToString());
            }
            catch (Exception e)
            {
                _logger.logIt(e.Message);
                return;
            }

            if (dllHandle == IntPtr.Zero)
            {
                _logger.logIt(@"can't load dll");
                return;
            }
            try
            {
                pfCreateLibrary = (DelegateI)Marshal.GetDelegateForFunctionPointer(
                                                GetProcAddress(dllHandle, "DPLCreateLibrary"), typeof(DelegateI));
                pfCreateBuffer = (DelegateBII)Marshal.GetDelegateForFunctionPointer(
                                                GetProcAddress(dllHandle, "DPLCreateBuffer"), typeof(DelegateBII));
                pfAddToBuffer = (DelegateIIBBI)Marshal.GetDelegateForFunctionPointer(
                                                GetProcAddress(dllHandle, "DPLAddToBuffer"), typeof(DelegateIIBBI));
                pfAddTrueTypeFontFromString = (DelegateIIB)Marshal.GetDelegateForFunctionPointer(
                                                GetProcAddress(dllHandle, "DPLAddTrueTypeFontFromString"), typeof(DelegateIIB));
                pfAddTrueTypeFontFromStringЕх = (DelegateIIBI)Marshal.GetDelegateForFunctionPointer(
                                                GetProcAddress(dllHandle, "DPLAddTrueTypeFontFromStringEx"), typeof(DelegateIIBI));

                pfAddTrueTypeFontFromFileEx = (DelegateIIWI)Marshal.GetDelegateForFunctionPointer(
                    GetProcAddress(dllHandle, "DPLAddTrueTypeFontFromFileEx"), typeof(DelegateIIWI));

                pfReleaseBuffer = (DelegateIIB)Marshal.GetDelegateForFunctionPointer(
                                                GetProcAddress(dllHandle, "DPLReleaseBuffer"), typeof(DelegateIIB));
                pfUnlockKey = (DelegateIIW)Marshal.GetDelegateForFunctionPointer(
                                                GetProcAddress(dllHandle, "DPLUnlockKey"), typeof(DelegateIIW));

                pfGetTempPath = (DelegateWI)Marshal.GetDelegateForFunctionPointer(
                    GetProcAddress(dllHandle, "DPLGetTempPath"), typeof(DelegateWI));
                pfLibraryStringResultLength = (DelegateII)Marshal.GetDelegateForFunctionPointer(
                    GetProcAddress(dllHandle, "DPLStringResultLength"), typeof(DelegateII));
                pfSetTempPath = (DelegateIIW)Marshal.GetDelegateForFunctionPointer(
                    GetProcAddress(dllHandle, "DPLSetTempPath"), typeof(DelegateIIW));

            }
            catch (Exception e)
            {
                _logger.logIt(@"err:" + e.Message);
            }

            AddTrueTypeFontFromString(signatureFontFile, libkey);
            Release();
        }

        private string SR(IntPtr data)
        {
            int size = pfLibraryStringResultLength(instanceID);
            byte[] result = new byte[size * 2];
            Marshal.Copy(data, result, 0, size * 2);
            return Encoding.Unicode.GetString(result);
        }

        private int AddTrueTypeFontFromString(String signatureFontFile, String libkey)
        {
            byte[] stream = File.ReadAllBytes(signatureFontFile);
            if (stream == null)
                _logger.logIt("failure reading font file to stream");
            else
                _logger.logIt("font file read, length: " + stream.Length.ToString());


            instanceID = pfCreateLibrary();
            int ilock = pfUnlockKey(instanceID, libkey);
            _logger.logIt("library unlocked with " + ilock.ToString());
            GCHandle gch = GCHandle.Alloc(stream, GCHandleType.Pinned);

            String temppath= SR(pfGetTempPath(instanceID));
            _logger.logIt("temp path: " + temppath);

            String newtmppath = @"c:\mib\temp";
            int res = pfSetTempPath(instanceID, newtmppath);
            _logger.logIt("temp path changed to: " + newtmppath + " " + (res == 1 ? "success" : "failure"));

            _logger.logIt("pinned mem alloc");
            IntPtr bufferID = pfCreateBuffer(instanceID, stream.Length);
            _logger.logIt(String.Format("buffer created, pointer: {0}", bufferID));
            pfAddToBuffer(instanceID, bufferID, gch.AddrOfPinnedObject(), stream.Length);
            _logger.logIt("source buffer copied");
            int result = 0;

            // following calls breaks in Azure only, and only if run as service, exe works normally with the same credentials.
            result = pfAddTrueTypeFontFromStringЕх(instanceID, bufferID,1);
            _logger.logIt(String.Format("font added using stream. Font id:{0} ", result));

            result = pfAddTrueTypeFontFromFileEx(instanceID, signatureFontFile, 1);
            _logger.logIt(String.Format("font added using file. File:{0} Font id:{1} ", signatureFontFile, result));

            pfReleaseBuffer(instanceID, bufferID);
            _logger.logIt("resource released");
            gch.Free();
            return result;
        }

    }
}
